Helpdesk for Drupal
===================

Copyright 2005-2011 Ouest Systemes Informatiques
Licensed under the GPL version 2 or later.


This module is designed to provide helpdesk support, typically in
a commercial setting oriented towards business consumers.

There has never been any working version (yet), and no code in the master
branch : like all Drupal.org projects, use one of the branches:

  * 4.6.x-1.x for the Drupal 4.6 branch
  * 6.x-1.x for the Drupal 6 branch
  * or use branch archaeology for even earlier recovered code.
